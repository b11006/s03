-- Create a database.
CREATE DATABASE blog_db;

-- users table
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

-- posts table
CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_posts_users_id 
        FOREIGN KEY (author_id) REFERENCES users(id) 
        ON UPDATE CASCADE 
        ON DELETE RESTRICT
);

-- post likes table
CREATE TABLE post_likes (
	id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_post_likes_users_id 
        FOREIGN KEY (user_id) REFERENCES users(id) 
        ON UPDATE CASCADE 
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_posts_id 
        FOREIGN KEY (post_id) REFERENCES posts(id) 
        ON UPDATE CASCADE 
        ON DELETE RESTRICT
);

-- post comments id
CREATE TABLE post_comments (
	id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_post_comments_users_id 
        FOREIGN KEY (user_id) REFERENCES users(id) 
        ON UPDATE CASCADE 
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_posts_id 
        FOREIGN KEY (post_id) REFERENCES posts(id) 
        ON UPDATE CASCADE 
        ON DELETE RESTRICT
);

-- insert users
INSERT INTO users(email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- insert posts
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (1 ,"First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (1 ,"Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (2 ,"Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (4 ,"Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- get title with user id = 1
SELECT title FROM posts WHERE author_id = 1;

-- get all the user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- update post's content to "hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete the user with an email of "jogndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";

